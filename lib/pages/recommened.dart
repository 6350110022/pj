import 'package:flutter/material.dart';

class RecommenedMenu extends StatelessWidget {
  const RecommenedMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[200],
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 50,bottom: 10),
            child: Icon(
              Icons.local_cafe,
              color: Colors.black,
              size: 100,
            ),
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("image/water1.jpg"),
              radius: 30,
            ),
            title: Text(
              'โกโก้ปั่น',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '100 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.shopping_basket,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("โกโก้ปั่น",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("สายช็อค ห้ามพลาด! เมนูสุดพิเศษ "),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "กลับ",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("image/water2.jpg"),
              radius: 30,
            ),
            title: Text(
              'มัทฉะกรีนทรี',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '120 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.shopping_basket,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("มัทฉะกรีนทรี",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("เป็นเมนู ซิกเนเจอร์ ! ของทางร้าน มีรสชาติที่เข้มข้น หวานและบอดี้เน้นกว่าชาเขียวปกติ กลิ่นหอมนวลละมุน เป็นเอกลักษณ์ของทางร้าน"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "กลับ",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("image/water3.jpg"),
              radius: 30,
            ),
            title: Text(
              'กาโกเย็น',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '120 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.shopping_basket,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("กาโกเย็น",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("โดยทางร้านของเรานำ โกโก้ผสมกับกาแฟ ให้รสชาติโกโก้มีกลิ่นหอมของกาแฟ อัดเเน่นไปด้วยความเข้มข้น หวานมันกำลังดี ต้องลองรับรองความอร่อย"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "กลับ",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("image/water4.jpg"),
              radius: 30,
            ),
            title: Text(
              'เอสเปรสโซ',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '80 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.shopping_basket,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("เอสเปรสโซ",style: TextStyle(fontWeight: FontWeight.bold),),
                    content: new Text("สาวกสายทำงาน ห้ามพลาด! เป็นกาแฟมีรสชาติแก่และเข้ม เป็นเครื่องดื่มมีส่วนผสมของนมสด ทำให้รสชาติเครื่องดื่มเข้มข้นและหอมนมสุดๆและด้านบนตบท้ายด้วยฟองนมนุ่มๆชิมแล้วต้องติดใจแน่นอน ได้สักแก้วก่อนไปทำงาน คือเริ่ด!!"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "กลับ",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("image/water5.jpg"),
              radius: 30,
            ),
            title: Text(
              'กาแฟส้ม',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '120 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.shopping_basket,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("กาแฟส้ม",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("เมนูยอดนิยมของทางร้าน! เป็นกาแฟที่ผสมผสานกับน้ำผลไม้ เหมาะสำหรับคนที่ชอบรสชาติขมแปลกใหม่แต่ลงตัว กินแล้วรู้สดชื่นและได้รับวิตามินซีสูง  "),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "กลับ",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("image/water6.jpg"),
              radius: 30,
            ),
            title: Text(
              'เเบล็คทรี',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '150 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.shopping_basket,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("แบร็คทรี",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("เป็นชาเมนูโบราณ! มีรสชาติหวานนำ กลิ่นหอมใบชาสกัดจากใบชาที่ดีที่สุด สดชื่นด้วยกลิ่นหอมมีกลิ่นความเป็นไทยที่โดดเด่น สามารถผสมกับน้ำมะนาวเพื่อเพิ่มรสชาติให้ละมุนมากยิ่งขึ้น"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "กลับ",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10,left: 140,right: 140),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.brown),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0),
                        ))),
                child: Text(
                  "Back",
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
                onPressed: () {
                  Navigator.pop(context,"...");
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
